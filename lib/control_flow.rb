# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  new_string = ""
  str.chars.each do |letter|
    new_string << letter if letter != letter.downcase
  end
  new_string
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  mid = str.length / 2
  if str.length.odd?
    return str[mid]
  end
  str[mid-1..mid]
end

# Return the number of vowels in a string.
#VOWELS = %w(a e i o u)
def num_vowels(str)
   vowels = ["a","e","i","o","u" ]
   counter = 0
    str.chars.each {|letter| counter += 1 if vowels.include?(letter)}
counter
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  counter = 1
  array = (1..num).to_a
    array.each do |i|
      counter *= i
  end
    counter
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
 join_string = ""
 arr.each_with_index do |word, indx|
   if word == arr.last
     join_string << word
   else join_string << word + separator
    end
 end
 join_string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  new_string = ""
  str.chars.each_with_index do |letter, index|
   if index == 0
     new_string << letter.downcase
   elsif index.even?
     new_string << letter.downcase
   else new_string << letter.upcase
   end
 end
    new_string
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
new_array = []
str.split.each do |word|
  if word.length >= 5
    new_array << word.reverse
  else new_array << word
  end
end
new_array.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
final = []
n_array = (1..n).to_a
n_array.each do |num|
  if num % 3 == 0 && num % 5 == 0
    final << "fizzbuzz"
  elsif num % 3 == 0
    final << "fizz"
  elsif num % 5 == 0
    final << "buzz"
  else final << num
  end
end
  final
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
 arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
 return false if num == 1
 (2..num/2).to_a.each do |n|
   return false if num % n == 0
 end
 true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
 factor = []
 (1..num).to_a.each do |n|
   factor << n if num % n == 0
 end
 factor.sort
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factor = []
  (1..num).to_a.each do |n|
    factor << n if num % n == 0 && prime?(n) == true
  end
  factor.sort
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length 
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def count_even(arr)
even_counter = 0
arr.each do |num|
  even_counter += 1 if num.even?
  end
  even_counter
end

def oddball(arr)
arr.each do |num|
  if count_even(arr) == 1
    return num if num.even?
  else return num if num.odd?
  end
end
end
